## Dockerfile For GNUKhata Based on Debian 10 (Buster)

Installs GNUKhata `v6.50`

### From docker hub

`docker pull kskarthik/gnukhata`
or visit https://hub.docker.com/r/kskarthik/gnukhata
### Manual Installation
- Install docker
- clone this repo `https://gitlab.com/kskarthik/build.git`
- To build docker image run `build-gnukhata.sh` 
- To run docker image run `start-gnukhata.sh`

Open `localhost:9000` in browser. You can use GNUKhata !

![GNU Khata Docker](screenshot.png "GNU Khata Docker")

While running `start-gnukhata.sh` for the first time, It creates three docker volumes 
named `gketc` `gklib` & `gklog` to store GNUKhata data.
 
