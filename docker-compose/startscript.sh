#!/bin/bash
echo "checking database status"
cd /gnukhata/gkcore
python3 initdb.py
echo "pulling latest gkcore changes ..."
git pull

echo "pulling latest gkwebapp changes ..."
cd /gnukhata/gkwebapp
git pull

echo "starting supervisor"
cd /gnukhata
supervisord -c supervisord.conf
tail -f /dev/null
